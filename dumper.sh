#!/bin/bash
source config.cfg

echo $db_user
echo $db_pass
echo $server
echo $port
echo $database
auth="${db_user}:${db_pass}"
base64=`echo -n $auth | base64`
curl -H "Accept: application/json" -H "Authorization: Basic $base64" -H "Content-Type: application/json" -s "http://${server}:${port}/${database}/_all_docs?include_docs=true" -o dump.txt


